const { ApolloServer } = require("apollo-server");
const { ApolloGateway, RemoteGraphQLDataSource } = require("@apollo/gateway");
const https = require("https");
const axios = require("axios");

class AuthenticatedDataSource extends RemoteGraphQLDataSource {
  willSendRequest({ request, context }) {
    const token = context.token;
    console.log(token);
    const agent = new https.Agent({
      rejectUnauthorized: false
    });

    try {
      return axios({
        httpsAgent: agent,
        method: "GET",
        url: "https://localhost:8081/api/usersettings/get",
        headers: {
          "content-type": "application/json",
          Cookie: `TKSESSIONID=${token};`
        }
      })
        .then(response => {
          console.log(response.data);
          request.http.headers.set("userId", response.data.user.id);
          request.http.headers.set("accountId", response.data.user.account.id);
        })
        .catch(err => {
          console.log(err);
        });
    } catch (e) {
      console.log("trycatch", e);
    }
  }
}

const gateway = new ApolloGateway({
  serviceList: [
    { name: "accounts", url: "http://localhost:4001/graphql" },
    { name: "reviews", url: "http://localhost:4002/graphql" },
    { name: "products", url: "http://localhost:4003/graphql" },
    { name: "inventory", url: "http://localhost:4004/graphql" }
  ],

  buildService({ name, url }) {
    return new AuthenticatedDataSource({ url });
  }
});

(async () => {
  const { schema, executor } = await gateway.load();

  const server = new ApolloServer({
    schema,
    executor,
    // Disable subscriptions (not currently supported with ApolloGateway)
    subscriptions: false,

    context: ({ req }) => {
      const token = "18972511-3147-44f2-a9c0-e96bd55d6bd5";
      return { token };
    }
  });

  server.listen().then(({ url }) => {
    console.log(`🚀 Server ready at ${url}`);
  });
})();
